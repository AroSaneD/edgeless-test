import { server, io } from "../app/utilities";

export { server, io } from "../app/utilities";

export const port = 3001;

export const startServer = () => {
    server.listen(port, function() {
        console.log("listening on *:3001");
    });
};

export const closerServer = ()=>{
    server.close()
}