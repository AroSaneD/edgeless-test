import * as assert from "assert";
import { of } from "rxjs";
import { first, filter, debounceTime, timeout, catchError} from "rxjs/operators";

import { io, server, port, startServer, closerServer } from "./server";

import { User, UserStorage } from "../app/user";
import { MainLobby } from "../app/lobby/mainLobby";
import { RoomFactory, RoomStorage, Room } from "../app/rooms";
import { UserFactory } from "../app/user/userFactory";
import { ClientSocketHelper } from "./clientSocketHelper";


let userStorage: UserStorage, roomStorage: RoomStorage, lobby: MainLobby, secretRoom: Room;

before(done => {
    userStorage = new UserStorage();
    roomStorage = new RoomStorage();

    const userFactory = new UserFactory();
    const roomFactory = new RoomFactory(io, () => roomStorage.rooms);

    roomStorage.addRoom(roomFactory.createDefaultRoom("1", "Default room", "default"));
    roomStorage.addRoom(roomFactory.createPublicRoom("2", "test room", "test"));
    secretRoom = roomFactory.createPrivateRoom("secret1", "Secret room", "secret");
    roomStorage.addRoom(secretRoom);

    lobby = new MainLobby(io, userStorage, roomStorage, userFactory, roomFactory);

    startServer();

    setTimeout(() => {
        done();
    }, 1500);
});

after(() => {
    closerServer();
    lobby.disconnectAllUsers();
});

describe("Users", () => {
    it("User should be able to connect and receive a greeting message", done => {
        const sender = new ClientSocketHelper("testUser1");
        sender.messageStream.pipe(first()).subscribe(msg => {
            done();
            sender.disconnect();
        });
    });

    it("After connecting, the user count in the system should increase", done => {
        const userCount = userStorage.users.length;
        const sender = new ClientSocketHelper("testUser2");

        sender.messageStream.pipe(first()).subscribe(msg => {
            assert.equal(userStorage.users.length, userCount + 1);
            done();
            sender.disconnect();
        });
    });

    it("After posting a message, a use should see his/her own message", done => {
        const sender = new ClientSocketHelper("testUser3");
        const testMessage = "zxcasd123_test!!";

        sender.emit(testMessage);
        sender.messageStream.pipe(filter(msg => msg.includes(testMessage))).subscribe(msg => {
            done();
            sender.disconnect();
        });
    });

    it("After connecting to the server, the users first message should set his/her name", done => {
        const user1Name = "testUser4_nameTest_toMakeSureNameIsUnique";
        const sender = new ClientSocketHelper(user1Name);

        sender.messageStream.pipe(debounceTime(100)).subscribe(() => {
            const currentUserExists = userStorage.users.some(u => u.name === user1Name);
            assert.equal(currentUserExists, true);

            done();
            sender.disconnect();
        });
    });

    it("If 2 users join, they should be able too see each others messages", done => {
        const user1Name = "testUser5";
        const sender = new ClientSocketHelper(user1Name);
        const testMessage = "zxcasd123_test!!";

        const user2Name = "testUser6";
        const sender1 = new ClientSocketHelper("testUser6");
        const testMessage1 = "zxcasd456_test!!";

        sender.emit(testMessage);
        sender1.emit(testMessage);

        const sender1ReceivedMessage = sender1.messageStream.pipe(filter(msg => msg.includes(testMessage)));
        const senderReceivedMessage = sender.messageStream.pipe(filter(msg => msg.includes(testMessage1)));

        sender1ReceivedMessage.subscribe(() => {
            sender1.emit(testMessage1);
        });

        senderReceivedMessage.subscribe(msg => {
            done();
            sender.disconnect();
            sender1.disconnect();
        });
    });

    it("When user enters 'secret', he should enter the 'Secret room'", done =>{
        const user1Name = "testUser7";
        const sender = new ClientSocketHelper(user1Name);
        sender.emit("secret");

        sender.messageStream.pipe(debounceTime(100)).subscribe(() => {
            const userExist = secretRoom.subscribedUsers.map(s => s.user).some(u => u.name === user1Name);
            assert.equal(userExist, true);

            done();
            sender.disconnect();
        });
    })

    it("When a user leaves a room, he/she should no longer be able to send/receive messages to/from other users in said room.", done => {
        const user1Name = "testUser8";
        const sender = new ClientSocketHelper(user1Name);
        const testMessage = "zxcasd123_test!!@3";

        const user2Name = "testUser9";
        const sender1 = new ClientSocketHelper(user2Name);
        const testMessage1 = "zxcasd456_test!!$2";

        sender.emit("secret");

        // Very long debounce time, because sometimes the user takes a while to be added to the room.
        sender.messageStream.pipe(debounceTime(500)).subscribe(() => {
            sender.emit(testMessage);

            sender1.messageStream
                .pipe(
                    filter(msg => msg.includes(testMessage)),
                    timeout(100),
                    catchError(error => of(-1))
                )
                .subscribe(msgOrNumber =>{
                    assert.equal(msgOrNumber, -1);
                    done();

                    sender.disconnect();
                    sender1.disconnect();
                })
        });
    });

});
