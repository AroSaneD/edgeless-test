import * as ioClient from "socket.io-client";
import { Subject } from "rxjs";

import { port } from "./server";

const ioOptions = {
    transports: ["websocket"],
    forceNew: true,
    reconnection: false
};

export class ClientSocketHelper {
    socket: any;
    messageStream: Subject<string>;

    constructor(public name: string = null) {
        this.socket = ioClient(`http://localhost:${port}/`, ioOptions);
        this.messageStream = new Subject<string>();

        this.socket.on("message", msg => {
            this.messageStream.next(msg);
        });

        // set name, if given
        if (name) {
            this.emit(name);
        }
    }

    emit(msg: string) {
        this.socket.emit("message", msg);
    }

    disconnect() {
        this.socket.disconnect();
    }
}
