# Edgeless test

This project was created for the edgless job selection (_lithuanian: "atranka"_) process.

# Deployment

To start the project, install the dependencies (dev included, for easier start) and run the start command. Sadly, project not available on npm (yet).

```
npm install 
npm start
```


# Note

The system was tested using __iocat__, instead of the recommended __wscat__, because my Windows machine didn't play nice with the latter. In order to connect, install either of these (for windows machines, I recommend iocat) and connect using it while the project server is launched.

```
iocat --socketio ws://localhost:3000
```

Also, if you want to use __wscat__, you will need a special connection string (for socket.io):
```
wscat -c ws://localhost:3000/socket.io/\?transport=websocket
``` 

[Links for iocat](https://www.npmjs.com/package/iocat)