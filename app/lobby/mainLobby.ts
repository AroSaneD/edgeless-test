import { Server } from "socket.io";

import { User, UserFactory, UserStorage } from "../user";
import { Room, RoomFactory, RoomStorage } from "../rooms";
import { SocketEvents } from "../utilities";
import { Subject, Observable, interval } from "rxjs";
import { map } from "rxjs/operators";

type connectionCallback = (u: User) => void;

export class MainLobby {
    constructor(
        private io: Server,
        private userStorage: UserStorage,
        private roomStorage: RoomStorage,
        private userFactory: UserFactory,
        private roomFactory: RoomFactory
    ) {
        this.io.on("connection", async socket => {
            const user = this.userFactory.createUser(socket);
            this.trackUser(user);

            const setupRoom = this.roomFactory.createSetupRoom(user);
            const selectedUserName = await setupRoom.getUserName(user);
            user.name = selectedUserName;
            this._greetUser(user);

            const defaultRoom = this.roomStorage.findDefaultRoom();
            defaultRoom.addUser(user);

            this.connectionStream.next(user);
        });
    }

    public connectionStream: Subject<User> = new Subject<User>();

    private _greetUser(user: User) {
        const roomAccessPhrases = this.roomStorage.getAcessPhrasesString();

        this.io
            .to(user.id)
            .emit(
                SocketEvents.MESSAGE_EVENT,
                `System message: Thank you, ${
                    user.name
                }. If you wish to join any other channel, simply type the name of the channel. The current choices are: ${roomAccessPhrases}.`
            );
    }

    private trackUser(user: User): void {
        this.userStorage.addUser(user);
        user.onDisconnect.subscribe(() => this.userStorage.removeUser(user));
    }

    // For test purposes, to make sure no remaining sockets prevent the process from ending.
    public disconnectAllUsers() {
        this.userStorage.users.forEach(u => u.disconnect());
    }
}
