import { User } from ".";

export class UserStorage {
    users: User[] = [];

    constructor() {}

    addUser(user: User) {
        this.users = [...this.users, user];
    }

    removeUser(user: User) {
        this.users = this.users.filter(u => u.id !== user.id);
    }
}
