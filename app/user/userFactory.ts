import { Socket } from "socket.io";
import { User, SocketUser } from ".";

export class UserFactory {
    constructor() {}

    createUser(socket: Socket): User {
        return new SocketUser(socket);
    }
}
