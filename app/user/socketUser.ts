import { Socket, Server } from "socket.io";
import { Subject, Observable } from "rxjs";

import { SocketEvents } from "../utilities";
import { User, UserMessageEvent } from "./user";

export class SocketUser implements User {
    public name: string;

    public get id(): string {
        return this.socket.id;
    }

    onMessage: Subject<UserMessageEvent> = new Subject();
    onDisconnect: Subject<User> = new Subject();

    constructor(public socket: Socket) {
        socket.on(SocketEvents.MESSAGE_EVENT, (message: string, data: any) => {
            this.onMessage.next(new UserMessageEvent(this, message));
        });

        socket.on(SocketEvents.DISCONNECT_EVENT, () => {
            this.onDisconnect.next(this);

            this.onDisconnect.complete();
            this.onMessage.complete();
        });
    }

    private _currentRoom: string;
    changeRoom(roomId: string): void {
        this.socket.leave(this._currentRoom);

        this.socket.join(roomId);
        this._currentRoom = roomId;
    }

    disconnect(): void {
        this.socket.disconnect();
    }
}
