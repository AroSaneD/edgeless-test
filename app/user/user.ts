import { Observable } from "rxjs";

export class UserMessageEvent {
    constructor(public user: User, public message: string) {}
}

export interface User {
    name: string;
    id: string;
    onMessage: Observable<UserMessageEvent>;
    onDisconnect: Observable<User>;
    changeRoom(roomId: string): void;
    disconnect(): void;
}