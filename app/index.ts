import { server, io } from "./utilities";

import { User, UserStorage } from "./user";
import { MainLobby } from "./lobby/mainLobby";
import { RoomFactory, RoomStorage } from "./rooms";
import { UserFactory } from "./user/userFactory";

const userStorage = new UserStorage();
const roomStorage = new RoomStorage();

const userFactory = new UserFactory();
const roomFactory = new RoomFactory(io, () => roomStorage.rooms);

roomStorage.addRoom(roomFactory.createDefaultRoom("1", "Default room", "default"));
roomStorage.addRoom(roomFactory.createPublicRoom("2", "test room", "test"));
roomStorage.addRoom(roomFactory.createPrivateRoom("secret1", "Secret room", "secret"));

const lobby = new MainLobby(io, userStorage, roomStorage, userFactory, roomFactory);

lobby.connectionStream.subscribe((user: User) => {
    console.log("New user joined the lobby");
});

server.listen(3000, function() {
    console.log("listening on *:3000");
});

// console commands:
//wscat -c ws://localhost:3000/socket.io/\?transport=websocket
//iocat --socketio ws://localhost:3000
