export enum SocketEvents{
    MESSAGE_EVENT = "message",
    DISCONNECT_EVENT = "disconnect"
}