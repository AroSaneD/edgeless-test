import * as express from "express";
import * as http from "http";
import * as socketio from "socket.io";

const app = express();

export const server = new http.Server(app);
export const io = socketio(server);;