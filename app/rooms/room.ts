import { Observable, Subject, Subscription, interval } from "rxjs";

import { User, UserMessageEvent } from "../user";
import { SocketEvents } from "../utilities";
import { Server } from "socket.io";
import { map, share } from "rxjs/operators";

class UserSubscription {
    constructor(
        public user: User,
        public messageSubscription: Subscription,
        public disconnectSubscription: Subscription
    ) {}
}

export class Room {
    isDefault = false;
    accessPhrase: string;
    isPrivate: boolean;

    subscribedUsers: UserSubscription[] = [];

    repeatingAnnouncement: Subscription;

    userMessageStream: Subject<UserMessageEvent> = new Subject();
    userAddedToRoomStream: Subject<User> = new Subject();
    userRemovedFromRoomStream: Subject<User> = new Subject();

    constructor(
        public id: string,
        public name: string,
        private io: Server,
        private availableRoomResolver: () => Room[]
    ) {
        this.repeatingAnnouncement = this.getAnnouncementStream().subscribe(message => this.broadcastMessage(message));
        this.userMessageStream.subscribe(event => this.onUserMessage(event));

        this.userAddedToRoomStream.subscribe(user => {
            this.broadcastMessage(this.playerGreeting(user), user.id);
            this.broadcastMessage(this.playerAnnouncement(user));
        });

        this.userRemovedFromRoomStream.subscribe(user => this.onUserLeft(user));
    }

    private getRandomNumber() {
        return Math.floor(Math.random() * 1000).toString();
    }

    protected getAnnouncementStream(): Observable<string> {
        const periodInMs = 5000;

        return interval(periodInMs).pipe(
            map(n => `${this.name}: ${this.getRandomNumber()}`),
            share()
        );
    }

    protected playerGreeting(user: User): string {
        return `System message: you have entered the '${this.name}' room.`;
    }

    protected playerAnnouncement(user: User): string {
        return `${this.name}: A new user has joined, please welcome '${user.id}'.`;
    }

    protected onUserMessage(event: UserMessageEvent) {
        const triedToAcessRoom = Room.FindRoomMatchingPhrase(event.message, this.availableRoomResolver());
        if (triedToAcessRoom) {
            this.swapRoom(event.user, triedToAcessRoom);
            return;
        }

        if (event.message == "count") {
            const userCount = this.subscribedUsers.length.toString();
            const countMessage = `${this.name}: There are currently ${userCount} users in this room.`;
            this.broadcastMessage(countMessage, event.user.id);
            return;
        }

        const emitString = `${event.user.name} said '${event.message}'`;
        this.broadcastMessage(emitString);
    }

    protected onUserLeft(user: User): void {
        this.broadcastMessage(`${this.name}: ${user.name} has left the room.`);
    }

    public addUser(newUser: User): void {
        newUser.changeRoom(this.id);

        const messageSubscription = newUser.onMessage.subscribe(event => this.userMessageStream.next(event));
        const disonnectSubscription = newUser.onDisconnect.subscribe(user => this.removeUser(user));

        this.subscribedUsers = [
            ...this.subscribedUsers,
            new UserSubscription(newUser, messageSubscription, disonnectSubscription)
        ];
        this.userAddedToRoomStream.next(newUser);
    }

    public removeUser(userToRemove: User): void {
        // Sacrificed efficiency for readability.
        // Can use findIndexOf and then splice it to reduce number of iterations.
        const subscriptionToRemove = this.subscribedUsers.find(s => s.user.id === userToRemove.id);
        subscriptionToRemove.messageSubscription.unsubscribe();
        subscriptionToRemove.disconnectSubscription.unsubscribe();

        this.subscribedUsers = this.subscribedUsers.filter(s => s.user.id !== userToRemove.id);

        this.userRemovedFromRoomStream.next(userToRemove);
    }

    public broadcastMessage(msg: string, recipientId: string = this.id): void {
        if (msg) {
            this.io.to(recipientId).emit(SocketEvents.MESSAGE_EVENT, msg);
        }
    }

    swapRoom(userToSwap: User, roomToSwapTo: Room) {
        this.removeUser(userToSwap);
        roomToSwapTo.addUser(userToSwap);
    }
    // STATIC FUNCTIONS

    public static CheckPhrase(phrase: string, room: Room): boolean {
        return room.accessPhrase && phrase === room.accessPhrase;
    }

    public static FindRoomMatchingPhrase(phrase: string, rooms: Room[]): Room {
        return rooms.find(r => this.CheckPhrase(phrase, r));
    }
}
