import { Room } from ".";

export class RoomStorage {
    rooms: Room[] = [];

    constructor() {}

    public addRoom(room: Room): void {
        if (this.rooms.some(r => r.id === room.id && r.name === room.name && r.accessPhrase === room.accessPhrase)) {
            console.error("Tried to add rom with matching id, name or passphrase");
            return;
        }

        this.rooms = [...this.rooms, room];
    }

    public addRooms(rooms: Room[]): void {
        rooms.forEach(r => this.addRoom(r));
    }

    public findDefaultRoom(): Room {
        return this.rooms.find(r => r.isDefault);
    }

    public getAcessPhrasesString(): string {
        return this.rooms
            .filter(r => !r.isPrivate && r.accessPhrase)
            .map(r => `'${r.accessPhrase}'`)
            .join(", ");
    }
}
