import { Server } from "socket.io";
import { Room } from ".";
import { User } from "../user";
import { PrivateSetupRoom } from "./privateSetupRoom";

export class RoomFactory {
    constructor(private io: Server, private roomResolver: () => Room[]) {}

    createPublicRoom(id: string, name: string, accessPhrase: string): Room {
        const room = new Room(id, name, this.io, this.roomResolver);

        room.accessPhrase = accessPhrase;
        room.isPrivate = false;

        return room;
    }

    createDefaultRoom(id: string, name: string, accessPhrase: string): Room {
        const room = this.createPublicRoom(id, name, accessPhrase);

        room.isDefault = true;

        return room;
    }

    createPrivateRoom(id: string, name: string, accessPhrase: string): Room {
        const room = new Room(id, name, this.io, this.roomResolver);

        room.accessPhrase = accessPhrase;
        room.isPrivate = true;

        return room;
    }

    createSetupRoom(user: User): PrivateSetupRoom {
        return new PrivateSetupRoom(user, this.io, this.roomResolver);
    }
}
