import { Server } from "socket.io";
import { first } from "rxjs/operators";

import { Room } from ".";
import { User, UserMessageEvent } from "../user";
import { Observable, empty } from "rxjs";

export class PrivateSetupRoom extends Room {
    constructor(user: User, io: Server, availableRoomResolver: () => Room[]) {
        super(`${user.id}_privateRoom`, "", io, availableRoomResolver);

        this.accessPhrase = null;
        this.isPrivate = true;
    }

    protected getAnnouncementStream(): Observable<string> {
        return empty();
    }

    protected playerGreeting(user: User): string {
        return "Welcome! Please enter the display name you wish to have.";
    }

    protected playerAnnouncement(user: User): string {
        return null;
    }

    protected onUserMessage(user: UserMessageEvent): void {
        return null;
    }

    protected onUserLeft(user: User): void {
        return null;
    }

    public async getUserName(user: User): Promise<string> {
        const promise = new Promise<string>(async (res, rej) => {
            this.addUser(user);
            const userEvent = await this.userMessageStream.pipe(first()).toPromise();
            this.removeUser(user);
            res(userEvent.message);
        });

        return promise;
    }
}
